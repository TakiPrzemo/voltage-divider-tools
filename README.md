# Voltage-Divider-Tools
A tool for calculating the best combination of resistors for a voltage divider with a set input and output voltage. <br /> <br />

USAGE: <br />
`
python3 voltage-divider-resistor-picker.py <Maxumim quisescent current> <Input Voltage> <Divider output voltage> <Available Resistances (E3/E6/E12/E24/E48/E96/E192/custom)> <OPTIONAL(only for custom resistances): Custom available resistances (in CSV format)>
`

I use this when designing DC-DC converter/linear regulator feedback loops and such, but your imagination's the limit!

## EXAMPLE1:<br />
 Let's say that we have a 5V input and aim to achieve a 1.8V output using E24 series resistors. Our goal is to determine the resistor combination that creates a divider the ouput of which comes as close to the desired 1.8V as possible, considering the available resistances. Additionally, we want to avoid drawing above 0.0001A from the 5V rail (when the output is at no load). 
In a scenario like this, the usage would be: <br />
`
python3 voltage-divider-resistor-picker.py 0.0001 5 1.8 E24
` <br />
Output: `Best resistor combination: R1(near Vin): 110000.0Ω, R2(near GND): 62000.0Ω, Vout: 1.802V, Deviation: 0.129%`<br />
<br />
## EXAMPLE2: <br />
Imagine a situation where you're stuck with some incomplete set of resistors and need to figure out the best combination to get a certain voltage output. For this, you can use the `custom` option. The usage would be:
`
python3 voltage-divider-resistor-picker.py 0.1 12 3.3 custom 100,200,470,5000,6700,1000000
` <br />
Output: `Best resistor combination: R1(near Vin): 470.0Ω, R2(near GND): 200.0Ω, Vout: 3.582V, Deviation: 8.548%`<br />
<br />

## to-do: 
-Create a pretty graphical interface, <br />
-Add a resistance from series/parallel resistors calculation tool <br />
-integrate the existing and above(to-do) tool to add support for resistances from parallel/series resistors (set a maximum "combination span (see if output is high or low, then brute force bottom/top resistances while checking the maximum current draw)"). <br />

